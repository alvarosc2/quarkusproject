package org.acme.resource;

import io.quarkus.test.junit.QuarkusTest;
import org.apache.http.HttpHeaders;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.CoreMatchers.*;

@QuarkusTest
class HolidaysResourceTest {

    @Test
    void getProcessingDatesTest1() {
        given()
                .contentType("application/json")
                .body("{\"businessDaysFromNow\": 1,\"arbitraryDate\": \"2020-10-01 11:10\" }")
                .when()
                .post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("bankBusinessDate", is("2020-10-02"))
                .body("calendarDaysFromNow", is(1))
                .body("businessDaysFromNow", is(1));
    }

    @Test
    void getProcessingDatesTest2() {
        given()
                .contentType("application/json")
                .body("{\"businessDaysFromNow\": 2,\"arbitraryDate\": \"2020-10-01 11:10\" }")
                .when()
                .post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("bankBusinessDate", is("2020-10-05"))
                .body("calendarDaysFromNow", is(4))
                .body("businessDaysFromNow", is(2));
    }

    @Test
    void getProcessingDatesTest3() {
        given()
                .contentType("application/json")
                .body("{\"businessDaysFromNow\": 2,\"arbitraryDate\": \"2020-10-09 20:59\" }")
                .when()
                .post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("bankBusinessDate", is("2020-10-14"))
                .body("calendarDaysFromNow", is(5))
                .body("businessDaysFromNow", is(2));
    }

    @Test
    void getProcessingDatesTest4() {
        given()
                .contentType("application/json")
                .body("{\"businessDaysFromNow\": 0,\"arbitraryDate\": \"2020-10-10 20:59\" }")
                .when()
                .post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("bankBusinessDate", is("2020-10-13"))
                .body("calendarDaysFromNow", is(3))
                .body("businessDaysFromNow", is(0));
    }

    @Test
    void getProcessingDatesTest5() {
        given()
                .contentType("application/json")
                .body("{\"businessDaysFromNow\": 3,\"arbitraryDate\": \"2020-11-09 20:59\" }")
                .when()
                .post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("bankBusinessDate", is("2020-11-13"))
                .body("calendarDaysFromNow", is(4))
                .body("businessDaysFromNow", is(3));
    }

    @Test
    void getProcessingDatesTest6() {
        given()
                .contentType("application/json")
                .body("{\"businessDaysFromNow\": -3,\"arbitraryDate\": \"2020-11-13 20:59\" }")
                .when()
                .post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("bankBusinessDate", is("2020-11-09"))
                .body("calendarDaysFromNow", is(-4))
                .body("businessDaysFromNow", is(-3));
    }
}
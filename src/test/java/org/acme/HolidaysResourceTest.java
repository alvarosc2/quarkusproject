package org.acme;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.acme.model.request.ProcessingDatesRequestModel;
import org.acme.model.response.ProcessingDatesResponseModel;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

@QuarkusTest
public class HolidaysResourceTest {
    @Test
    public void testProcessingArbitraryDateEndpoint() {
        /*
          {
             "bankBusinessDate": "2020-09-17",
             "businessDaysFromNow": 7,
             "calendarDaysFromNow": 9
          }
        */

        ProcessingDatesRequestModel processingDatesRequestModel = new ProcessingDatesRequestModel();
        ProcessingDatesResponseModel processingDatesResponseModel = new ProcessingDatesResponseModel();

        processingDatesRequestModel.setArbitraryDate("2020-09-07 11:10");
        processingDatesRequestModel.setBusinessDaysFromNow(7);

        processingDatesResponseModel.setBankBusinessDate("2020-09-17");
        processingDatesResponseModel.setBusinessDaysFromNow(7);
        processingDatesResponseModel.setCalendarDaysFromNow(9);

        given()
                .contentType(ContentType.JSON)
                .body(processingDatesRequestModel)
                .when().post("/v1/processing-arbitrary-date")
                .then()
                .statusCode(200)
                .body("businessDaysFromNow", is(7))
                .body("calendarDaysFromNow", is(9))
                .body("bankBusinessDate", is("2020-09-17"));
    }
}

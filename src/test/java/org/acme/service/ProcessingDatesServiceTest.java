package org.acme.service;

import io.quarkus.test.junit.QuarkusTest;
import org.acme.model.request.ProcessingDatesRequestModel;
import org.acme.model.response.ProcessingDatesResponseModel;
import org.acme.util.IDateCacheUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@QuarkusTest
class ProcessingDatesServiceTest {

    @Inject
    ProcessingDatesService processingDatesService;

    @BeforeEach
    void setUp() {
    }

    @Test
    void calculateRelativeProcessingDateTest1() {
        IDateCacheUtil dateCacheUtil = mock(IDateCacheUtil.class);
        when(dateCacheUtil.getBankHolidaysList()).thenReturn(getBankHolidaysList());

        ProcessingDatesRequestModel processingDatesRequestModel = new ProcessingDatesRequestModel();
        processingDatesRequestModel.setArbitraryDate("2021-10-14 10:00");
        processingDatesRequestModel.setBusinessDaysFromNow(2);

        ProcessingDatesResponseModel processingDatesResponseModel = processingDatesService.calculateRelativeProcessingDate(processingDatesRequestModel);
        String actualBankBusinessDate = processingDatesResponseModel.getBankBusinessDate();
        String expectedBankBusinessDate = "2021-10-18";

        assertEquals(actualBankBusinessDate, expectedBankBusinessDate);
    }

    @Test
    void calculateRelativeProcessingDateTest2() {
        String arbitraryDate = "2021-10-14 21:00";
        String expectedBankBusinessDate = "2021-10-19";

        IDateCacheUtil dateCacheUtil = mock(IDateCacheUtil.class);
        when(dateCacheUtil.getBankHolidaysList()).thenReturn(getBankHolidaysList());

        ProcessingDatesRequestModel processingDatesRequestModel = new ProcessingDatesRequestModel();
        processingDatesRequestModel.setArbitraryDate(arbitraryDate);
        processingDatesRequestModel.setBusinessDaysFromNow(2);

        ProcessingDatesResponseModel processingDatesResponseModel = processingDatesService.calculateRelativeProcessingDate(processingDatesRequestModel);
        String actualBankBusinessDate = processingDatesResponseModel.getBankBusinessDate();
        
        assertEquals(actualBankBusinessDate, expectedBankBusinessDate);
    }

    private List<LocalDate> getBankHolidaysList() {
        List<LocalDate> bankHolidaysList = new LinkedList<>();
        bankHolidaysList.add(LocalDate.parse("2021-01-01"));

        return bankHolidaysList;
    }


}
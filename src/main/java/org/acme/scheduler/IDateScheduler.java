package org.acme.scheduler;

import java.util.List;

public interface IDateScheduler {
    List<String> getBankHolidays();
}

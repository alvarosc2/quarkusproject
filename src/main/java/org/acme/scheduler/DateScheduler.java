package org.acme.scheduler;

import io.quarkus.scheduler.Scheduled;
import org.acme.external.IBankHolidaysService;
import org.acme.model.response.BankHolidayModel;
import org.acme.model.response.HolidayDetailsModel;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReferenceArray;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class DateScheduler implements IDateScheduler {
    @Inject
    @RestClient
    IBankHolidaysService bankHolidaysService;

    private AtomicReferenceArray<String> bankHolidays;

    @Scheduled(every="600s")
    public void queryBankHolidaysService() {
        List<HolidayDetailsModel> holidayDetails;

        BankHolidayModel bankHolidayModel = bankHolidaysService.getBankHolidaysData("47b0e5cc-8492-496f-8169-8029cf626ef1", "US", "2020", "true");
        holidayDetails = bankHolidayModel.getHolidays();

        List<String> observedBankHolidaysList = new LinkedList<>();

        for (HolidayDetailsModel hdm: holidayDetails) {
            observedBankHolidaysList.add(hdm.getObserved());
        }

        String[] observedBankHolidaysArray = observedBankHolidaysList.stream().toArray(String[] :: new);

        bankHolidays = new AtomicReferenceArray<>(observedBankHolidaysArray);

        for (int i = 0; i < holidayDetails.size(); i++) {
            bankHolidays.set(i, holidayDetails.get(i).getObserved());
        }
    }

    @Override
    public List<String> getBankHolidays() {
        List<String> bankHolidayList = new LinkedList<>();

        for (int i = 0; i < bankHolidays.length(); i++){
            bankHolidayList.add(bankHolidays.get(i));
        }

        return bankHolidayList;
    }

}

package org.acme.service;

import org.acme.model.request.ProcessingDatesRequestModel;
import org.acme.model.response.ProcessingDatesResponseModel;

public interface IProcessingDatesService {
    ProcessingDatesResponseModel calculateRelativeProcessingDate(ProcessingDatesRequestModel processingDatesRequestModel);
}

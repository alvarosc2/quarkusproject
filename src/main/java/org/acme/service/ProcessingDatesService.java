package org.acme.service;

import net.bytebuddy.asm.Advice;
import org.acme.model.request.ProcessingDatesRequestModel;
import org.acme.model.response.ProcessingDatesResponseModel;
import org.acme.util.IDateCacheUtil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

import static java.time.temporal.ChronoUnit.DAYS;

@ApplicationScoped
public class ProcessingDatesService implements IProcessingDatesService {

    @Inject
    IDateCacheUtil dateCacheUtil;

    private LocalDateTime currentDate;

    private LocalDateTime bankBusinessDate;
    private int businessDaysFromNow;
    private int calendarDaysFromNow;

    private final String dateTimePattern = "yyyy-MM-dd hh:mm";
    private final int MAX_NUMBER_OF_BUSINESS_DAYS = 250;
    private final int MIN_NUMBER_OF_BUSINESS_DAYS = -250;

    private List<LocalDate> bankHolidaysDateList;

    @NotNull
    public ProcessingDatesResponseModel calculateRelativeProcessingDate(ProcessingDatesRequestModel processingDatesRequestModel) throws DateTimeParseException, IllegalArgumentException {

        String arbitraryDate = processingDatesRequestModel.getArbitraryDate();
        this.businessDaysFromNow = processingDatesRequestModel.getBusinessDaysFromNow();

        bankHolidaysDateList = dateCacheUtil.getBankHolidaysList();

        //What to do if the number of business days exceeds 250 days
        LocalDateTime actualDate = LocalDateTime.parse(arbitraryDate, DateTimeFormatter.ofPattern(this.dateTimePattern));
        LocalDateTime.now().format(DateTimeFormatter.ofPattern(this.dateTimePattern));

        long calendarDaysFromNow = DAYS.between(LocalDateTime.now(), actualDate);

        if (calendarDaysFromNow >= MIN_NUMBER_OF_BUSINESS_DAYS && calendarDaysFromNow <= MAX_NUMBER_OF_BUSINESS_DAYS) {
            processDate(actualDate);
        }
        else {
            throw new IllegalArgumentException();
        }

        return new ProcessingDatesResponseModel(bankBusinessDate.toLocalDate().toString(), this.businessDaysFromNow, this.calendarDaysFromNow);
    }

    private void processDate(LocalDateTime actualDate) {
        //Is already 21 hours? add one day to the current date else do not modify current date

        LocalDateTime cDate;
        if (isAlready21hours(actualDate)) {
            cDate = actualDate.plusDays(1);
        }
        else
        {
            cDate = actualDate;
        }

        if (businessDaysFromNow >= 0){
            // Adjust currentDate
            this.currentDate = calculateBusinessDate(cDate, true);

            // Calculate bankBusinessDate, businessDaysFromNow and calendarDaysFromNow
            this.bankBusinessDate = calculateBankBusinessDate(this.currentDate, true);
        }
        else {
            // Calculate currentDate
            this.currentDate = calculateBusinessDate(cDate, false);

            // Calculate bankBusinessDate, businessDaysFromNow and calendarDaysFromNow
            this.bankBusinessDate = calculateBankBusinessDate(this.currentDate, false);
        }
    }

    @NotNull
    private LocalDateTime calculateBusinessDate(LocalDateTime date, boolean forward) {
        boolean flag = true;

        int amount;
        if (forward){
            amount = 1;
        }
        else {
            amount = -1;
        }

        int daysToAdd = 0;
        while (flag) {
            String dayOfTheWeek = date.plusDays(daysToAdd * amount).getDayOfWeek().toString();

            switch (dayOfTheWeek) {
                case "MONDAY":
                case "TUESDAY":
                case "WEDNESDAY":
                case "THURSDAY":
                case "FRIDAY":
                    if (isBankHoliday(date.plusDays(daysToAdd * amount))) {
                        daysToAdd++;
                    }
                    else {
                        flag = false;
                    }

                    break;

                case "SATURDAY":
                case "SUNDAY":
                    daysToAdd++;
                    break;

                default:
                    System.out.println("Error");
                    break;
            }
        }

        return date.plusDays(daysToAdd * amount);
    }

    @NotNull
    private LocalDateTime calculateBankBusinessDate(LocalDateTime date, boolean forward) {

        int amount;
        if (forward){
            amount = 1;
        }
        else {
            amount = -1;
        }

        int counter = 1;
        int daysToAdd = 1;
        while (counter <= Math.abs(this.businessDaysFromNow)) {
            String dayOfTheWeek = date.plusDays(daysToAdd * amount).getDayOfWeek().toString();

            switch (dayOfTheWeek) {
                case "MONDAY":
                case "TUESDAY":
                case "WEDNESDAY":
                case "THURSDAY":
                case "FRIDAY":
                    if (isBankHoliday(date.plusDays(daysToAdd * amount))) {
                        daysToAdd++; //increments everyday
                    }
                    else {
                        daysToAdd++;
                        counter++; // increments only on working days
                    }

                    break;

                case "SATURDAY":
                case "SUNDAY":
                    daysToAdd++;
                    break;

                default:
                    System.out.println("Error");
                    break;
            }

        }

        this.calendarDaysFromNow = (daysToAdd - 1) * amount;

        return date.plusDays(this.calendarDaysFromNow);
    }

    private boolean isAlready21hours(LocalDateTime date) {
        boolean result;

        int hourOfTheDay = date.getHour();

        if (hourOfTheDay >= 21){
            result = true;
        }
        else {
            result = false;
        }

        return result;
    }

    private boolean isBankHoliday(LocalDateTime date) {
        boolean result = false;

        for (LocalDate bankHoliday: bankHolidaysDateList) {
            String strBankHoliday = bankHoliday.toString();
            String strDate = date.toLocalDate().toString();

            if (strBankHoliday.equals(strDate)) {
                result = true;
                break;
            }
            else {
                result = false;
            }
        }

        return result;
    }
}

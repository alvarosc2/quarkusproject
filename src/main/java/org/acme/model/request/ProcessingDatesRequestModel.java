package org.acme.model.request;

public class ProcessingDatesRequestModel {
    private int businessDaysFromNow;
    private String arbitraryDate;

    public int getBusinessDaysFromNow() {
        return businessDaysFromNow;
    }

    public void setBusinessDaysFromNow(int businessDaysFromNow) {
        this.businessDaysFromNow = businessDaysFromNow;
    }

    public String getArbitraryDate() {
        return arbitraryDate;
    }

    public void setArbitraryDate(String arbitraryDate) {
        this.arbitraryDate = arbitraryDate;
    }
}

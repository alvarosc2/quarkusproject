package org.acme.model.response;

public class WeekDayModel {
    DateModel date;
    ObservedModel observed;

    public DateModel getDate() {
        return date;
    }

    public void setDate(DateModel date) {
        this.date = date;
    }

    public ObservedModel getObserved() {
        return observed;
    }

    public void setObserved(ObservedModel observed) {
        this.observed = observed;
    }
}

package org.acme.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HolidayDetailsModel {
    String name;
    String date;
    String observed;
    @JsonProperty("public")
    String _public;
    String country;
    String uuid;
    WeekDayModel weekday;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getObserved() {
        return observed;
    }

    public void setObserved(String observed) {
        this.observed = observed;
    }

    public String get_public() {
        return _public;
    }

    public void set_public(String _public) {
        this._public = _public;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public WeekDayModel getWeekday() {
        return weekday;
    }

    public void setWeekday(WeekDayModel weekday) {
        this.weekday = weekday;
    }
}

package org.acme.model.response;

public class RequestsModel {
    int used;
    int available;
    String resets;

    public int getUsed() {
        return used;
    }

    public void setUsed(int used) {
        this.used = used;
    }

    public int getAvailable() {
        return available;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    public String getResets() {
        return resets;
    }

    public void setResets(String resets) {
        this.resets = resets;
    }
}

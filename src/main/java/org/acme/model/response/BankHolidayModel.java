package org.acme.model.response;

import java.util.List;

public class BankHolidayModel {
    int status;
    RequestsModel requests;
    List<HolidayDetailsModel> holidays;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RequestsModel getRequests() {
        return requests;
    }

    public void setRequests(RequestsModel requests) {
        this.requests = requests;
    }

    public List<HolidayDetailsModel> getHolidays() {
        return holidays;
    }

    public void setHolidays(List<HolidayDetailsModel> holidays) {
        this.holidays = holidays;
    }
}

package org.acme.model.response;

public class ProcessingDatesResponseModel {
    private String bankBusinessDate;
    private int businessDaysFromNow;
    private int calendarDaysFromNow;

    public ProcessingDatesResponseModel() {

    }

    public ProcessingDatesResponseModel(String bankBusinessDate, int businessDaysFromNow, int calendarDaysFromNow) {
        this.bankBusinessDate = bankBusinessDate;
        this.businessDaysFromNow = businessDaysFromNow;
        this.calendarDaysFromNow = calendarDaysFromNow;
    }

    public String getBankBusinessDate() {
        return bankBusinessDate;
    }

    public void setBankBusinessDate(String bankBusinessDate) {
        this.bankBusinessDate = bankBusinessDate;
    }

    public int getBusinessDaysFromNow() {
        return businessDaysFromNow;
    }

    public void setBusinessDaysFromNow(int businessDaysFromNow) {
        this.businessDaysFromNow = businessDaysFromNow;
    }

    public int getCalendarDaysFromNow() {
        return calendarDaysFromNow;
    }

    public void setCalendarDaysFromNow(int calendarDaysFromNow) {
        this.calendarDaysFromNow = calendarDaysFromNow;
    }
}

package org.acme.external;

import org.acme.model.response.BankHolidayModel;
import org.eclipse.microprofile.rest.client.annotation.RegisterClientHeaders;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/v1")
@RegisterRestClient
@RegisterClientHeaders
public interface IBankHolidaysService {
    @GET
    @Path("/holidays")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    BankHolidayModel getBankHolidaysData(
            @QueryParam("key") String key,
            @QueryParam("country") String country,
            @QueryParam("year") String year,
            @QueryParam("public") String publicHolidays
    );
}

package org.acme.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface IDateCacheUtil {
    List<LocalDate> getBankHolidaysList();
}

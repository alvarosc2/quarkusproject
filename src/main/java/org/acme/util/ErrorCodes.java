package org.acme.util;

import java.util.HashMap;
import java.util.Map;

public class ErrorCodes {
    private static Map<Long, String> errorTable;

    static {
        errorTable = new HashMap<>();

        errorTable.put(1001L, "IllegalArgumentException");
        errorTable.put(1002L, "DateTimeParseException");
        errorTable.put(1003L, "DateTimeException");
    }

    public static String getErrorMessage(long errorCode) {
        return errorTable.get(errorCode);
    }
}

package org.acme.util;

import org.acme.scheduler.DateScheduler;

import javax.enterprise.context.ApplicationScoped;

import javax.inject.Inject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


@ApplicationScoped
public class DateCacheUtil implements IDateCacheUtil {
    @Inject
    DateScheduler dateScheduler;

    private List<LocalDate> bankHolidaysDateList;

    public List<LocalDate> getBankHolidaysList() {
        bankHolidaysDateList = new LinkedList<>();
        List<String> bankHolidaysFromCache = dateScheduler.getBankHolidays();

        for (String date : bankHolidaysFromCache) {
            this.bankHolidaysDateList.add(LocalDate.parse(date));
        }

        return this.bankHolidaysDateList;
    }
}

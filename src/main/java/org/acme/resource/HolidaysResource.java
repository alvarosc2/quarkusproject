package org.acme.resource;

import org.acme.model.request.ProcessingDatesRequestModel;
import org.acme.model.response.ProcessingDatesResponseModel;
import org.acme.service.IProcessingDatesService;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.DateTimeException;
import java.time.format.DateTimeParseException;

import static org.acme.util.ErrorCodes.*;

@Path("/v1")
public class HolidaysResource {
    @Inject
    IProcessingDatesService processingDatesService;

    @POST
    @Path("/processing-arbitrary-date")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getProcessingDates(ProcessingDatesRequestModel processingDatesRequestModel) {
        try {
            ProcessingDatesResponseModel processingDatesResponseModel = processingDatesService.calculateRelativeProcessingDate(processingDatesRequestModel);
            return Response.ok(processingDatesResponseModel).build();
        }
        catch (IllegalArgumentException e) {
            return Response.ok("[" + 1001L + "] " + getErrorMessage(1001L) + e.getMessage()).build();
        }
        catch (DateTimeParseException e) {
            return Response.ok("[" + 1002L + "] " + getErrorMessage(1002L) + e.getMessage()).build();
        }
        catch (DateTimeException e) {
            return Response.ok("[" + 1003L + "] " + getErrorMessage(1003L) + e.getMessage()).build();
        }
    }
}
